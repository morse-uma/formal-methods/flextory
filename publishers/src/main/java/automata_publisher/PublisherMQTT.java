package automata_publisher;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;


public class PublisherMQTT {
	public static void main(String[] args) {
		String topic = "dash";
		int qos = 2;
		String broker = "tcp://localhost";
		String clientId;

		try {
			clientId = MqttClient.generateClientId();
			MqttClient sampleClient = new MqttClient(broker, clientId);
			MqttConnectOptions connOpts = new MqttConnectOptions();
			// connOpts.setCleanSession(true);
			// connOpts.setUserName("");
			// connOpts.setPassword("".toCharArray());
			System.out.println("Connecting to broker: " + broker);

			sampleClient.connect(connOpts);
			System.out.println("Connected");

			/******** Read dash traces ******************/
			/*******************************************
			 * C1: timestamp C2: un número que representa la combinación de flags tcp
			 * activos en el paquete. Events present in the system trace syn = 2, rst = 4,
			 * ack = 10, fin_ack = 11, syn_ack = 12, rst_ack=14, psh_ack=18, fin_psh_ack=19
			 * 
			 * 
			 * C3: source tcp port C4: target tcp port C5: iRTT (en ms creo) C6: Length C7:
			 * window size C8: sequence number C9: ack number
			 */
			String DELIMITER = "\\s+";
			String TXT_FILE = "events.txt";

			List<List<String>> records = new ArrayList<>();
			try (BufferedReader br = new BufferedReader(new FileReader(TXT_FILE))) {
				String line;
				while ((line = br.readLine()) != null) {
					String[] values = line.split(DELIMITER);
					records.add(Arrays.asList(values));
					// System.out
					// .println(Stream.of(values).map(Object::toString).collect(Collectors.joining("\n")).toString());
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			List<Integer> ports = new ArrayList<>();
			if (!records.isEmpty()) {
				records.stream().forEach(r -> {
					var sourcePort = Integer.parseInt(r.get(3));
					var targetPort = Integer.parseInt(r.get(4));
					if (sourcePort != 443 && sourcePort != 56514) {
						if (!ports.contains(sourcePort)) {
							ports.add(sourcePort);
						}
					} else if (targetPort != 443 && targetPort != 56514) {
						if (!ports.contains(targetPort)) {
							ports.add(targetPort);
						}
					}
				});
			}

			if (!records.isEmpty()) {
				for (int port : ports) {
					for (List<String> r : records) {
						if (Integer.parseInt(r.get(3)) == port || Integer.parseInt(r.get(4)) == port) {
							String json = "[\n";
							json += "{" + "\"timestamp\"" + " : \"" + r.get(0) + " " + r.get(1) + "\"," + "\"flags\""
									+ " : " + r.get(2) + "," + "\"source\"" + " : " + r.get(3) + "," + "\"target\""
									+ " : " + r.get(4) + "," + "\"seqN\"" + " : " + r.get(8) + "}";
							json += "\n]";
							/********************************** Send data ******************************/
							MqttMessage message = new MqttMessage(json.getBytes());
							message.setQos(qos);
							sampleClient.publish(topic, message);
							System.out.println(json.toString());
							Thread.sleep(1000);
						}
					}
				}
			}

			sampleClient.disconnect();
			sampleClient.close();
			System.out.println("Disconnected");
			System.exit(0);
		} catch (MqttException me) {
			System.out.println("reason " + me.getReasonCode());
			System.out.println("msg " + me.getMessage());
			System.out.println("loc " + me.getLocalizedMessage());
			System.out.println("cause " + me.getCause());
			System.out.println("excep " + me);
			me.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
