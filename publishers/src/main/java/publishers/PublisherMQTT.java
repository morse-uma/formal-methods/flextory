package publishers;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class PublisherMQTT {
	public static void main(String[] args) {
	    String topic        = "iris";
	    int qos             = 2;
	    String broker       = "tcp://localhost";
	    String clientId;
	    
	    try {
	    	clientId = MqttClient.generateClientId();
	    	
	        MqttClient sampleClient = new MqttClient(broker, clientId);
	        MqttConnectOptions connOpts = new MqttConnectOptions();
	        //connOpts.setCleanSession(true);
	        //connOpts.setUserName("");
	        //connOpts.setPassword("".toCharArray());
	        System.out.println("Connecting to broker: "+broker);
	        
	        sampleClient.connect(connOpts);
	        System.out.println("Connected");
	        
	        FileInputStream jsonData;
			String json = null;
			try {
				jsonData = new FileInputStream("iris.json");
				json = IOUtils.toString(jsonData, StandardCharsets.UTF_8);
			} catch (IOException e) {
				e.printStackTrace();
			}
	        
	        MqttMessage message = new MqttMessage(json.getBytes());
	        message.setQos(qos);
	        sampleClient.publish(topic, message);
	        System.out.println("Message published");
	        
	        sampleClient.disconnect();
	        System.out.println("Disconnected");
	        System.exit(0);
	    } catch(MqttException me) {
	        System.out.println("reason "+me.getReasonCode());
	        System.out.println("msg "+me.getMessage());
	        System.out.println("loc "+me.getLocalizedMessage());
	        System.out.println("cause "+me.getCause());
	        System.out.println("excep "+me);
	        me.printStackTrace();
	    }
	}

}
