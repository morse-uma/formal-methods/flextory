# Flextory

## Table of contents
* [Description](#description)
* [Technologies](#technologies)
* [Installation](#installation)
* [Usage of Flextory](#usage-of-flextory)
* [Usage of FLEX-consumers](#usage-of-flex-consumer)

## Description
*Flextory* is a tool that eases the development of *IoT Consumer applications* that are responsible for receiving and processing data.
The Consumer applications make use of an intermediate message broker which is responsible for the distribution and management of data. 
*Flextory* can automatically build a Consumer application that connects to the remote message broker, read the data with the format specified by the user, and process the data using the algorithm alos provided by the Flextory user. The consumer application produced with *Flextory* are called as *FLEX-consumers*.

## Technologies
Project has been created with:
* Java JDK 17.0.2 2022-01-18 LTS
* commons-cli 1.5.0
* commons-io 2.14.0
* amqp-client 5.19.0
* eclipse-paho 1.2.5
* jackson-annotation 2.15.3
* jackson-core 2.15.3
* jackson-databind 2.15.3
* slf4j-api 2.0.9
* slf4j-simple 2.0.9

## Installation

*Flextory*  has been tested on Windows 64 and Ubuntu 64 distributions.

### From source code
Pre-requisite: Java Development Kit (JDK) and Maven.

Flextory can be generated from source code (code folder) using Maven. 

### From binary
Pre-requisite: Java Development Kit (JDK) 17.

Flextory binary (bin folder) does not require installation.


## Usage of Flextory

### Open Flextory
It is recommended to invoke Flextory using a command-line. The command is: 

`java -jar (Path to Flextory)/Flextory.jar`

![Open Flextory](images/OpenFlextory.PNG)

### Step 1: Type the FLEX-consumer name
The first step using Flextory consist in choosing a proper FLEX-consumer name. The introduced name has to start with the letter in Uppercase and it can not be followed by an extension (e.g .java)

![Step 1](images/step1.PNG)

### Step 2: Enter the JSON schema
In this step the user has to introduce a JSON schema that has a description about the data that will be received by the FLEX-consumer. The following example shows a simple JSON schema used to describe the Iris json data:

```json
{
	"title": "Iris Flower",
	"type": "object",
	"properties": {
		"sepal_length": {
			"type": "number"
		},
		"sepal_width": {
			"type": "number"
		},
		"petal_length": {
			"type": "number"
		},
		"petal_width": {
			"type": "number"
		},
		"species": {
			"type": "string"
		}
	}
}
```

To know more about JSON schemas you can enter to this [website](https://json-schema.org/)

And this is the resulted java class:

```java
public class IrisSchema {
	
	...

    @JsonProperty("sepal_length")
    private Double sepalLength;
    @JsonProperty("sepal_width")
    private Double sepalWidth;
    @JsonProperty("petal_length")
    private Double petalLength;
    @JsonProperty("petal_width")
    private Double petalWidth;
    @JsonProperty("species")
    private String species;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("sepal_length")
    public Double getSepalLength() {
        return sepalLength;
    }

    @JsonProperty("sepal_length")
    public void setSepalLength(Double sepalLength) {
        this.sepalLength = sepalLength;
    }

	...
```

These java classes are created as POJO clases using [jsonschema2pojo](https://www.jsonschema2pojo.org/) plugin, which has rules of conversion and creation of getter and setter methods.

In Flextory, the user only has to search the JSON schema (.json extension) file using the chooser:

![Step 2](images/step2.PNG)

### Step 3: Save the algorithm template
In this step the user has to decide where to save an algorithm template that has to be completed with the user´s algorithm. In particular, the method "run()" is the one that needs to be filled. As it can be seen in the code below, there is an ArrayList that will store the "IrisSchema" data (result of the conversion of the json data, received by the FLEX-consumer, to an instance of the Java class).

```java
public class Algorithm implements Runnable {
	LinkedBlockingQueue<IrisSchema> data;
	
	public Algorithm(LinkedBlockingQueue<IrisSchema> data){
		this.data = data;
	}

	@Override
	public void run() {
        ...
	} 
}
```

### Step 4: Enter completed algorithm class
In this step the user has to search the completed Algorithm class using the chooser. This is an example of a complete algorithm class for Iris FLEX-consumer demo: 
```java
public class Algorithm implements Runnable {
	LinkedBlockingQueue<IrisSchema> data;
	
	public Algorithm(LinkedBlockingQueue<IrisSchema> data){
		this.data = data;
	}

	@Override
	public void run() {
		float virginica = 0;
		float versicolor = 0;
		float setosa = 0;
		float other = 0;
		for (IrisSchema flower : data) {
			switch (flower.getSpecies()) {
			case "setosa":
				setosa++;
				break;
			case "versicolor":
				versicolor++;
				break;
			case "virginica":
				virginica++;
				break;
			default:
				other++;
			}
		}
		System.out.println("Virginica: " + virginica / data.size() * 100 + "%");
		System.out.println("Versicolor: " + versicolor / data.size() * 100 + "%");
		System.out.println("Setosa: " + setosa / data.size() * 100 + "%");
		System.out.println("Null: " + other / data.size() * 100 + "%");	
	} 
}
```

### Step 5: Choose externals Jars
In this step the user can choose between adding or not external JARs to compile the FLEX-consumer algorithm. In case of a positive choice, Flextory will display a window to manage the new imports:

![Step 5](images/step5choose.PNG)

### Step 6: Select communication protocol
In the current step, Flextory offers two protocols, MQTT and AMQP, and the selected one will be used by the resulted FLEX-consumer to communicate with the message broker.

### Step 7: Save FLEX-consumer
In this last step the user decide the directory where the FLEX-consumer is going to be created.

<!--Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README. -->

## Usage of FLEX-consumer
FLEX-consumers are stand-alone applications that only need the Java Development Kit to start. Nevertheless, the purpose of them is to connect to a message broker (messaging server), for this reason is mandatory to have one like [RabbitMQ](https://www.rabbitmq.com/) (This is the broker tested in the project, we can not ensure the compatibility with others).

### Open a FLEX-consumer
To execute a FLEX-consumer it is necessary to open a command-line and type: 

`java -jar (Path to consumer)/(consumer name) (arguments)`

There are two forms of introducing arguments, the short and the full version, e.g -t and --topic. These are all the arguments availables:

- **-h, --help**: Display a help text.
- **-ip, --host**: Host IP address. (*Mandatory*)
- **-t, --topic (only MQTT)**: The name of the topic that the FLEX-consumer will create and subscribe in the message broker. (*Mandatory*)
- **-q, --queue (only AMQP)**: The name of the queue that the FLEX-consumer will create and subscribe in the message broker. (*Mandatory*)
- **-p, --port**: Listening port of the broker, by default the FLEX-consumer will use the protocol`s default ports (5672 for AMQP and 1883 for MQTT). (*Optional*)
- **-u, --user**: Username in authenticated connections. (*Optional*)
- **-pass, --password**: Password in authenticated connections. (*Optional*)
- **-d, --delused**: Flag to delete used messages (received messages will be proceseed only once by the algorithm). (*Optional*)
- **-fe, --finalexec**: Flag to enable a final execution when the connection is closed in case there are unprocessed messages (Deadctivated by default) (This flag is ignored when there is no repetition). (*Optional*)
- **-max, --maxmessages**: Maximum number of waited messages. (*Optional*)
- **-mr, --messagerep**: Consumer will repeat the algorithm every time that x number of messages arrive. (*Optional*)
- **-tr, --timerep**: Interval of algorithm execution in minutes. (*Optional*)
- **-w, --wait**: Time in minutes that FLEX-consumer is going to wait for a new message. (*Optional*).
- **-pers, --persistent**:  One algorithm execution during all the FLEX-consumer life. FLEX-consumer will execution the processing algorithm at the moment of its invocation. 

There are invalid argument configurations, the following figure shows all the possible combinations:

[<img src="images/restrictions.PNG" width="500" />](restrictions.PNG)

The dashed vertical lines represent the separation between "levels" and it is interpreted from left ro right. The arguments of the same level are exclusive (you can use them together), for example, --timerep and --messagerep can not go at the same time.
The symbol ___ indicates an empty option (not using an argument) in that level.
The colored horizontal lines show the separation between branches of configurations (current level to the end).

One possible reading would be to use the argument --delused, then select --timerep, after select --wait, and finally use --finalexec.

There is not an specific order to introduce the arguments.

The persistent option only allow an ending condition.

### Iris FLEX-consumer demo
The image below serves as an ilustrative example of executing a FLEX-consumer of the Iris use case. It uses MQTT protocol, the broker address is localhost (located in the same machine), the topic to subscribe is test and we want the FLEX-consumer to repeat the algorithm with every data received.

![Iris FLEX-consumer](images/consumer.PNG)

Data has to be formatted as JSON



## Authors and acknowledgment
Flextory is the result of the graduation project of Rafael López Gómez and coordinated by Laura Panizo and María del Mar Gallardo.

## License
This project is under GNU AFFERO GENERAL PUBLIC LICENSE

