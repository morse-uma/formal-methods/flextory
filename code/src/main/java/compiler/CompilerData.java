package compiler;

import java.util.ArrayList;

public class CompilerData {
	public String className;
	public String schemaName;
	public String schemaPath;
	public String templatePath;
	public String algorithmPath;
	public String consumerPath;
	public String protocol;
	public ArrayList<String> pathJars;
	
}