package compiler;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

import org.jsonschema2pojo.DefaultGenerationConfig;
import org.jsonschema2pojo.GenerationConfig;
import org.jsonschema2pojo.Jackson2Annotator;
import org.jsonschema2pojo.SchemaGenerator;
import org.jsonschema2pojo.SchemaMapper;
import org.jsonschema2pojo.SchemaStore;
import org.jsonschema2pojo.rules.RuleFactory;

import com.sun.codemodel.JCodeModel;

import gui.ResultPanel;

public class Compiler extends SwingWorker <String, String> {
	private CompilerData data;
	private ResultPanel resultPanel;	
	private String consumer;
	private String result;

	public Compiler(CompilerData data, ResultPanel resultPanel) {
		this.data = data;
		this.resultPanel = resultPanel;
	}

	@Override
	protected String doInBackground() throws Exception {
		// Path to save consumer
		Path finalPath = Paths.get(data.consumerPath);
		
		/*******************
		 * POJO CLASSES CREATION
		 *******************/
		
		File jsonSchemaFile = new File(data.schemaPath);
		File jsonSchemaCopy = new File(finalPath + "/" + data.schemaName + ".json");
		try {
			Files.copy(jsonSchemaFile.toPath(), jsonSchemaCopy.toPath(),
					StandardCopyOption.REPLACE_EXISTING);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			JCodeModel jcodeModel = new JCodeModel();
		    GenerationConfig config = new DefaultGenerationConfig() {
		        @Override
		        public boolean isGenerateBuilders() {
		            return true;
		        }
		    };
		    SchemaMapper mapper = new SchemaMapper(new RuleFactory(config, 
		    		new Jackson2Annotator(config), new SchemaStore()), new SchemaGenerator());
		    mapper.generate(jcodeModel, "", "", jsonSchemaCopy.toURI().toURL());
		 
		    jcodeModel.build(finalPath.toFile());
		} catch (Exception e) {
			e.printStackTrace();
		}
		publish("POJO classes created");
		
		/*******************
		 * ALGORITHM COPY
		 *******************/
		File algorithmFile = new File(data.algorithmPath);
		File algorithmCopy = new File(finalPath + "/Algorithm.java");
		try {
			Files.copy(algorithmFile.toPath(), algorithmCopy.toPath(),
					StandardCopyOption.REPLACE_EXISTING);
		} catch (Exception e) {
			e.printStackTrace();
		}
		publish("Algorithm class created");
		
		/*******************
		 * CONSUMER CREATION
		 *******************/
		// Main consumer class path
		Path javaFilePath = Paths.get(finalPath.normalize().toAbsolutePath().toString(), data.className + ".java");
		
		try {
			if(data.protocol == "MQTT") {
				consumer = ConsumerTemplates.getMQTT(data.className, data.schemaName);
			}else {
				consumer = ConsumerTemplates.getAMQP(data.className, data.schemaName);
			}
			//Override existing class if exists
			Files.write(javaFilePath, consumer.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		publish("Main class created");
		
		/*************
		 * COPY JARs and MANIFEST CREATION
		 *************/
		ArrayList<String> nedeedJars = new ArrayList<String>();
		nedeedJars.add("amqp-client-5.19.0.jar");
		nedeedJars.add("commons-cli-1.5.0.jar");
		nedeedJars.add("jackson-annotations-2.15.3.jar");
		nedeedJars.add("jackson-core-2.15.3.jar");
		nedeedJars.add("jackson-databind-2.15.3.jar");
		nedeedJars.add("slf4j-api-2.0.9.jar");
		nedeedJars.add("slf4j-simple-2.0.9.jar");
		nedeedJars.add("org.eclipse.paho.client.mqttv3-1.2.5.jar");
		
		try {
			//Create libs directory to store JARs
			Path pathLibs = Paths.get(finalPath + "/libs");
			Files.createDirectories(pathLibs);
			ArrayList<String> allJars = new ArrayList<String>();
			//Copy Jars
			for(String name : nedeedJars) {
				if(data.protocol.equals("MQTT")) {
					if(!name.equals("amqp-client-5.19.0.jar") && !name.equals("slf4j-api-2.0.9.jar")
							&& !name.equals("slf4j-simple-2.0.9.jar")) {
						InputStream source = this.getClass().getClassLoader().getResourceAsStream("resources/"+name); //"resources/"+name To find the file inside JAR
						File jarCopy = new File(pathLibs + "/" + name);
						Files.copy(source, jarCopy.toPath(), StandardCopyOption.REPLACE_EXISTING);
						allJars.add(name);
					}
				}else {
					if(!name.equals("org.eclipse.paho.client.mqttv3-1.2.5.jar")) {
						InputStream source = this.getClass().getClassLoader().getResourceAsStream("resources/"+name); //"resources/"+name To find the file inside JAR
						File jarCopy = new File(pathLibs + "/" + name);
						Files.copy(source, jarCopy.toPath(), StandardCopyOption.REPLACE_EXISTING);
						allJars.add(name);
					}
				}
			}
			if (data.pathJars != null) {
				for(String pathstr : data.pathJars) {
					Path origin = Paths.get(pathstr);
					File jarCopy = new File(pathLibs + "/" + origin.getFileName());
					Files.copy(origin, jarCopy.toPath(), StandardCopyOption.REPLACE_EXISTING);
					allJars.add(origin.getFileName().toString());
				}
			}
				
			//Building MANIFEST
			Path pathManifest = Paths.get(finalPath + "/MANIFEST.mf");
			StringBuilder manifest = new StringBuilder();
			manifest.append("Main-Class: " + data.className).append("\n");
			manifest.append("Class-Path: ");
			for(String jar : allJars) {
				manifest.append("libs/"+jar + " ");
			}
			manifest.append("\n");
			Files.write(pathManifest, manifest.toString().getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
		publish("/libs directory created");
		/*************
		 * COMPILATION
		 *************/ 
		String os = checkOperatingSystem();
		Process p = null;
		
		try {
			if(os.equals("windows")) {
				p = Runtime.getRuntime().exec("cmd");
			}else if(os.equals("linux")) {
				p = Runtime.getRuntime().exec("/bin/sh");
			}else if(os.equals("mac")) {
				p = Runtime.getRuntime().exec("/bin/sh");
			}
			
			//Printwriter to write commands
			PrintWriter stdin = new PrintWriter(p.getOutputStream());
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader error = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			
			// Enter commands o cmd
			stdin.println("cd " + finalPath);
			if(os.equals("windows")) {
				stdin.println("cd /d " + finalPath);
				stdin.println("javac -cp libs/* *.java");
				stdin.println("jar cmf MANIFEST.mf "+data.className+".jar *.class");
			}else if(os.equals("linux")) {
				stdin.println("cd " + finalPath);
				stdin.println("javac -cp libs/*:. *.java");
				stdin.println("ls");
				stdin.println("jar cmf MANIFEST.mf "+data.className+".jar *.class");
			}else if(os.equals("mac")) {
				stdin.println("cd " + finalPath);
				stdin.println("javac -cp libs/*:. *.java");
				stdin.println("ls");
				stdin.println("jar cmf MANIFEST.mf "+data.className+".jar *.class");
			}
		
			stdin.close();
			p.waitFor();
			StringBuilder output = new StringBuilder();
			String line = null;
			
			while ((line = reader.readLine()) != null) {
				output.append(line + "\n");
			}
		
			while ((line = error.readLine()) != null) {
				output.append(line + "\n");
			}
			result = output.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	private String checkOperatingSystem() {
        String so = System.getProperty("os.name").toLowerCase();
        if (so.contains("win")) {
           return "windows";
        } else if (so.contains("nix") || so.contains("nux")
                || so.contains("aix")) {
           return "linux";
        } else if (so.contains("mac")) {
            return "mac";
        }
        return null;
	}
	
	protected void done() {
		try {
			resultPanel.showResult(get()); 
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
	
	protected void process(String output) {
		resultPanel.showResult(output);
	}
}



