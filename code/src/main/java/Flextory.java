import java.awt.EventQueue;
import controller.Controller;
import gui.MainFrame;

public class Flextory {
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame gui = new MainFrame();
					Controller controller = new Controller(gui);  
					gui.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	

}
