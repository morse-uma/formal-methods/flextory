package controller;

import gui.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import compiler.Compiler;
import compiler.CompilerData;

public class Controller implements ActionListener {
	private MainFrame gui;

	private Step1Panel step1Panel;
	private Step2Panel step2Panel;
	private Step3Panel step3Panel;
	private Step4Panel step4Panel;
	private Step5Panel step5Panel;
	private SelectJarsPanel selectJarsPanel;
	private Step6Panel step6Panel;
	private Step7Panel step7Panel;
	private ResultPanel resultPanel;
	
	private CompilerData compilerData;
	
	public Controller(MainFrame gui) {
		this.gui = gui;
		
		compilerData = new CompilerData();
		
		step1Panel = new Step1Panel();
		step1Panel.setController(this);

		step2Panel = new Step2Panel();
		step2Panel.setController(this);

		step3Panel = new Step3Panel();
		step3Panel.setController(this);

		step4Panel = new Step4Panel();
		step4Panel.setController(this);

		step5Panel = new Step5Panel();
		step5Panel.setContoller(this);

		selectJarsPanel = new SelectJarsPanel();
		selectJarsPanel.setController(this);
		
		step6Panel = new Step6Panel();
		step6Panel.setController(this);

		step7Panel = new Step7Panel();
		step7Panel.setController(this);
		
		resultPanel = new ResultPanel();

		gui.changePanel(step1Panel);
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		if (evt.getActionCommand().equals(step1Panel.CONFIRMNAME)) {
			compilerData.className = step1Panel.getAppName();
			if (!compilerData.className.equals("")) {
				String regex = ".+\\..+";
				if (compilerData.className.charAt(0) == compilerData.className.toUpperCase().charAt(0)) {
					if (!compilerData.className.matches(regex)) {
						compilerData.schemaName = compilerData.className + "Schema";
						gui.changePanel(step2Panel);
						gui.changeStep(2);
					} else {
						step1Panel.setIncorrectName("Do not include extension");
					}
				}  else {
					step1Panel.setIncorrectName("First letter must go in uppercase");
				}
			}
		} else if (evt.getActionCommand().equals(step2Panel.SELECTSCHEMA)) {
			compilerData.schemaPath = step2Panel.searchSchema();
		} else if (evt.getActionCommand().equals(step2Panel.CONFIRMSCHEMAPATH)) {
			if (compilerData.schemaPath != null) {
				gui.changePanel(step3Panel);
				gui.changeStep(3);
			} else {
				step2Panel.setErrorPath();
			}
		} else if (evt.getActionCommand().equals(step3Panel.SELECTTEMPLATESAVE)) {
			compilerData.templatePath = step3Panel.searchAlgorithmTemplate();
		} else if (evt.getActionCommand().equals(step3Panel.CONFIRMTEMPLATESAVE)) {
			if (compilerData.templatePath != null) {
				Path algorithmFile = Paths.get(compilerData.templatePath, "Algorithm.java");
				String algorithm = "import java.util.concurrent.LinkedBlockingQueue;\r\n"
						+ "\r\n"
						+ "public class Algorithm implements Runnable {\r\n"
						+ "	LinkedBlockingQueue<" +compilerData.schemaName+ "> data;\r\n"
						+ "	\r\n"
						+ "	public Algorithm(LinkedBlockingQueue<" +compilerData.schemaName+ "> data){\r\n"
						+ "		this.data = data;\r\n"
						+ "	}\r\n"
						+ "\r\n"
						+ "	@Override\r\n"
						+ "	public void run() {\r\n"
						+ "	} \r\n"
						+ "}\r\n";
				try {
					Files.write(algorithmFile, algorithm.getBytes());
				} catch (Exception e) {
					e.printStackTrace();
				}
				gui.changePanel(step4Panel);
				gui.changeStep(4);
			} else {
				step3Panel.setErrorPath();
			}
		} else if (evt.getActionCommand().equals(step4Panel.SELECTALGORITHM)) {
			compilerData.algorithmPath = step4Panel.searchAlgorithm();
		} else if (evt.getActionCommand().equals(step4Panel.CONFIRMALGOTIRHM)) {
			if (compilerData.algorithmPath != null) {
				gui.changePanel(step5Panel);
				gui.changeStep(5);
			} else {
				step4Panel.setErrorPath();
			}
		} else if (evt.getActionCommand().equals(step5Panel.USEJARS)) {
			gui.changePanel(selectJarsPanel);
		} else if (evt.getActionCommand().equals(step5Panel.DONTUSEJARS)) {
			gui.changePanel(step6Panel);
			gui.changeStep(6);
		} else if (evt.getActionCommand().equals(selectJarsPanel.SEARCHJARS)) {
			selectJarsPanel.chooseJars();
		} else if (evt.getActionCommand().equals(selectJarsPanel.CANCELJARS)) {
			selectJarsPanel.cancelLastSelection();
		} else if (evt.getActionCommand().equals(selectJarsPanel.ENDSEARCHJARS)) {
			compilerData.pathJars = selectJarsPanel.getJars();
			gui.changePanel(step6Panel);
			gui.changeStep(6);
		} else if (evt.getActionCommand().equals(step6Panel.CONFIRMAMQP)) {
			compilerData.protocol = "AMQP";
			gui.changePanel(step7Panel);
			gui.changeStep(7);
		} else if (evt.getActionCommand().equals(step6Panel.CONFIRMMQTT)) {
			compilerData.protocol = "MQTT";
			gui.changePanel(step7Panel);
			gui.changeStep(7);
		} else if (evt.getActionCommand().equals(step7Panel.SELECTCONSUMERPATH)) {
			compilerData.consumerPath = step7Panel.searchConsumerPath();
		} else if (evt.getActionCommand().equals(step7Panel.CONFIRMCONSUMERPATH)) {
			if (compilerData.consumerPath != null) {
				try {
					Compiler c = new Compiler(compilerData, resultPanel);
					gui.changePanel(resultPanel);
					c.execute();
					
				} catch (Exception e) {
					e.printStackTrace();
				} 
			} else {
				step7Panel.setErrorPath();
			}
		}
	}
}
