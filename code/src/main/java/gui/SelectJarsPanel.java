package gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JList;
import javax.swing.JScrollPane;

@SuppressWarnings("serial")
public class SelectJarsPanel extends JPanel {
	private JLabel searchJarsLabel;
	private JLabel tip;
	
	private JButton searchJarsButton;
	private JButton cancelJarsButton;
	private JButton endSearchButton;
	
	private JList<Object> list;
	private JScrollPane scrollJars;

	private JSeparator separator;
	
	private ArrayList<String> pathJarsTemp, pathJars;
	
	public final String SEARCHJARS = "searchJars";
	public final String CANCELJARS = "cancelJars";
	public final String ENDSEARCHJARS = "endSearchJars";
	
	public SelectJarsPanel() {
		setBackground(Color.BLACK);
		setForeground(new java.awt.Color(248, 249, 251));
		setLayout(null);
		
		searchJarsLabel = new JLabel("Choose external JARs");
		searchJarsLabel.setForeground(Color.WHITE);
		searchJarsLabel.setHorizontalAlignment(SwingConstants.CENTER);
		searchJarsLabel.setFont(new Font("Segoe UI", Font.BOLD, 18));
		searchJarsLabel.setBounds(10, 11, 392, 29);
		add(searchJarsLabel);
		
		searchJarsButton = new JButton("Open chooser");
		searchJarsButton.setForeground(Color.WHITE);
		searchJarsButton.setBackground(Color.GRAY);
		searchJarsButton.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		searchJarsButton.setBounds(73, 219, 127, 23);
		add(searchJarsButton);
		
		separator = new JSeparator();
		separator.setBackground(Color.DARK_GRAY);
		separator.setForeground(Color.DARK_GRAY);
		separator.setBounds(10, 253, 392, 2);
		add(separator);
		
		tip = new JLabel("Press SHIFT to select multiple JARs within the same directory");
		tip.setForeground(Color.WHITE);
		tip.setFont(new Font("Segoe UI Light", Font.PLAIN, 11));
		tip.setHorizontalAlignment(SwingConstants.CENTER);
		tip.setBounds(20, 37, 382, 14);
		add(tip);
		
		cancelJarsButton = new JButton("Cancel last");
		cancelJarsButton.setForeground(Color.WHITE);
		cancelJarsButton.setBackground(Color.GRAY);
		cancelJarsButton.setEnabled(false);
		cancelJarsButton.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		cancelJarsButton.setBounds(210, 219, 127, 23);
		add(cancelJarsButton);
		
		endSearchButton = new JButton("End");
		endSearchButton.setBackground(Color.GRAY);
		endSearchButton.setForeground(Color.WHITE);
		endSearchButton.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		endSearchButton.setBounds(136, 266, 127, 23);
		add(endSearchButton);

		
		list = new JList<>();
		list.setBackground(Color.LIGHT_GRAY);
		list.setEnabled(false);
		scrollJars = new JScrollPane(list, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollJars.setBounds(10, 51, 392, 160);
		add(scrollJars);
		
		pathJars = new ArrayList<String>();
		
		setVisible(true);
	}
	
	public void setController(ActionListener c) {		
		searchJarsButton.addActionListener(c);
		searchJarsButton.setActionCommand(SEARCHJARS);
		
		cancelJarsButton.addActionListener(c);
		cancelJarsButton.setActionCommand(CANCELJARS);
		
		endSearchButton.addActionListener(c);
		endSearchButton.setActionCommand(ENDSEARCHJARS);
	}
	
	public void chooseJars() {
		JFileChooser chooser = new JFileChooser();
		chooser.setMultiSelectionEnabled(true);
		int option = chooser.showOpenDialog(this);
		if (option == JFileChooser.APPROVE_OPTION) { 
			File[] jars = chooser.getSelectedFiles();
			pathJarsTemp = new ArrayList<String>();
			for(File jar : jars) {
				pathJarsTemp.add(jar.getAbsolutePath());
			}
			pathJars.addAll(pathJarsTemp);
			list.setListData(pathJars.toArray());
			cancelJarsButton.setEnabled(true);
		}
	}
	
	public void cancelLastSelection() {
		pathJars.removeAll(pathJarsTemp);
		list.setListData(pathJars.toArray());
		cancelJarsButton.setEnabled(false);
	}
	
	public ArrayList<String> getJars() {
		return pathJars;
	}
}
