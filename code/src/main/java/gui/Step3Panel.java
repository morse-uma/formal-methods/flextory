package gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class Step3Panel extends JPanel {
	private JLabel saveTemplateLabel;
	private JLabel emptyPathErrorLabel;
	
	private JButton showTemplatePath;
	private JButton saveTemplateButton;
	private JButton confirmTemplateSaveButton;
	
	private JSeparator separator;
	
	private String templatePath;
	public final String SELECTTEMPLATESAVE = "selectTemplateSave";
	public final String CONFIRMTEMPLATESAVE = "confirmTemplateSave";

	public Step3Panel() {
		setBackground(Color.BLACK);
		setForeground(Color.WHITE);
		setLayout(null);
		
		saveTemplateLabel = new JLabel("Choose the path to save the algorithm template");
		saveTemplateLabel.setForeground(Color.WHITE);
		saveTemplateLabel.setHorizontalAlignment(SwingConstants.CENTER);
		saveTemplateLabel.setFont(new Font("Segoe UI", Font.BOLD, 14));
		saveTemplateLabel.setBounds(10, 11, 392, 52);
		add(saveTemplateLabel);
		
		showTemplatePath = new JButton("Path");
		showTemplatePath.setBackground(Color.BLACK);
		showTemplatePath.setForeground(Color.WHITE);
		showTemplatePath.setFont(new Font("Segoe UI Light", Font.PLAIN, 14));
		showTemplatePath.setEnabled(false);
		showTemplatePath.setBounds(10, 74, 392, 23);
		add(showTemplatePath);
		
		saveTemplateButton = new JButton("Open chooser");
		saveTemplateButton.setForeground(Color.WHITE);
		saveTemplateButton.setBackground(Color.GRAY);
		saveTemplateButton.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		saveTemplateButton.setBounds(10, 126, 127, 23);
		add(saveTemplateButton);
		
		separator = new JSeparator();
		separator.setBackground(Color.DARK_GRAY);
		separator.setForeground(Color.DARK_GRAY);
		separator.setBounds(10, 175, 392, 2);
		add(separator);
		
		confirmTemplateSaveButton = new JButton("Confirm");
		confirmTemplateSaveButton.setBackground(Color.GRAY);
		confirmTemplateSaveButton.setForeground(Color.WHITE);
		confirmTemplateSaveButton.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		confirmTemplateSaveButton.setBounds(136, 266, 127, 23);
		add(confirmTemplateSaveButton);
		
		emptyPathErrorLabel = new JLabel("Use the chooser to save the algorithm template");
		emptyPathErrorLabel.setForeground(Color.RED);
		emptyPathErrorLabel.setHorizontalAlignment(SwingConstants.CENTER);
		emptyPathErrorLabel.setFont(new Font("Segoe UI Black", Font.BOLD, 12));
		emptyPathErrorLabel.setBackground(Color.RED);
		emptyPathErrorLabel.setBounds(10, 213, 392, 23);
		emptyPathErrorLabel.setVisible(false);
		add(emptyPathErrorLabel);
		setVisible(true);
	}
	
	public void setController(ActionListener c) {
		saveTemplateButton.addActionListener(c);
		saveTemplateButton.setActionCommand(SELECTTEMPLATESAVE);
		
		confirmTemplateSaveButton.addActionListener(c);
		confirmTemplateSaveButton.setActionCommand(CONFIRMTEMPLATESAVE);
	}
	
	public String searchAlgorithmTemplate() {
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setDialogTitle("Choose where to save the template");
		int option = chooser.showSaveDialog(this);
		if (option == JFileChooser.APPROVE_OPTION) { 
			templatePath = chooser.getSelectedFile().getAbsolutePath(); 
			showTemplatePath.setText(templatePath);
			emptyPathErrorLabel.setVisible(false);
		}
		return templatePath;
	}

	public void setErrorPath() {
		emptyPathErrorLabel.setVisible(true);	
	}
	
}
