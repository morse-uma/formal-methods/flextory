package gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;

@SuppressWarnings("serial")
public class Step5Panel extends JPanel {
	private JLabel useJarsLabel;
	
	private JButton useJarsButton;
	private JButton dontUseJarsButton;
	
	public final String USEJARS = "useJars";
	public final String DONTUSEJARS = "dontUseJars";

	public Step5Panel() {
		setBackground(Color.BLACK);
		setForeground(new java.awt.Color(248, 249, 251));
		setLayout(null);
		
		useJarsLabel = new JLabel("Use external Jars?");
		useJarsLabel.setForeground(Color.WHITE);
		useJarsLabel.setHorizontalAlignment(SwingConstants.CENTER);
		useJarsLabel.setFont(new Font("Segoe UI", Font.BOLD, 18));
		useJarsLabel.setBounds(10, 11, 392, 52);
		add(useJarsLabel);
		
		useJarsButton = new JButton("Yes");
		useJarsButton.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		useJarsButton.setBounds(78, 266, 127, 23);
		add(useJarsButton);
		
		dontUseJarsButton = new JButton("No");
		dontUseJarsButton.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		dontUseJarsButton.setBounds(208, 266, 127, 23);
		add(dontUseJarsButton);
		
		JLabel imageJar = new JLabel("");
		imageJar.setForeground(Color.WHITE);
		imageJar.setBackground(Color.WHITE);
		imageJar.setIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage(this.getClass().getClassLoader().getResource("resources/icon-jar.png")))); // "resources/icon-jar.png" if compressed in JAR
		imageJar.setBounds(135, 59, 144, 196);
		add(imageJar);
		setVisible(true);
	}
	
	public void setContoller(ActionListener c) {
		useJarsButton.addActionListener(c);
		useJarsButton.setActionCommand(USEJARS);
		
		dontUseJarsButton.addActionListener(c);
		dontUseJarsButton.setActionCommand(DONTUSEJARS);
	}
	
}
