package gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class Step1Panel extends JPanel {
	private JLabel nameLabel;
	private JLabel namingErrorsLabel;
	
	private JButton confirmNameButton;
	
	private JTextField nameTextInput;
	
	public final String CONFIRMNAME = "confirmName";

	public Step1Panel() {
		setBackground(Color.BLACK);
		setForeground(new java.awt.Color(248, 249, 251));
		setLayout(null);
		
		nameLabel = new JLabel("Type the application name");
		nameLabel.setForeground(Color.WHITE);
		nameLabel.setFont(new Font("Segoe UI", Font.BOLD, 18));
		nameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		nameLabel.setBounds(10, 11, 392, 52);
		add(nameLabel);
		
		nameTextInput = new JTextField();
		nameTextInput.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		nameTextInput.setBounds(10, 84, 392, 20);
		add(nameTextInput);
		nameTextInput.setColumns(10);
		
		confirmNameButton = new JButton("Confirm");
		confirmNameButton.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		confirmNameButton.setForeground(Color.WHITE);
		confirmNameButton.setBackground(Color.GRAY);
		confirmNameButton.setBounds(136, 266, 127, 23);
		add(confirmNameButton);
		
		namingErrorsLabel = new JLabel("");
		namingErrorsLabel.setForeground(Color.RED);
		namingErrorsLabel.setHorizontalAlignment(SwingConstants.CENTER);
		namingErrorsLabel.setFont(new Font("Segoe UI", Font.BOLD, 18));
		namingErrorsLabel.setBackground(Color.RED);
		namingErrorsLabel.setBounds(10, 179, 392, 33);
		namingErrorsLabel.setVisible(false);
		add(namingErrorsLabel);
		setVisible(true);
	}
	
	public void setController(ActionListener c) {
		confirmNameButton.addActionListener(c);
		confirmNameButton.setActionCommand(CONFIRMNAME);
	}
	
	public String getAppName() {
		return nameTextInput.getText().trim();
	}
	
	public void setIncorrectName(String reason) {
		namingErrorsLabel.setText(reason);
		namingErrorsLabel.setVisible(true);
	}
}
