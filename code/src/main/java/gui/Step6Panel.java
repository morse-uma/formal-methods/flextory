package gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class Step6Panel extends JPanel {
	private JLabel protocolLabel;
	
	private JButton mqttButton;
	private JButton amqpButton;
	
	public final String CONFIRMMQTT = "confirmMQTT";
	public final String CONFIRMAMQP = "confirmAMQP";
	
	public Step6Panel() {
		setBackground(Color.BLACK);
		setForeground(new java.awt.Color(248, 249, 251));
		setLayout(null);
		
		protocolLabel = new JLabel("Select a communication protocol");
		protocolLabel.setForeground(Color.WHITE);
		protocolLabel.setHorizontalAlignment(SwingConstants.CENTER);
		protocolLabel.setFont(new Font("Segoe UI", Font.BOLD, 18));
		protocolLabel.setBounds(10, 11, 392, 52);
		add(protocolLabel);
		
		mqttButton = new JButton("MQTT");
		mqttButton.setForeground(Color.WHITE);
		mqttButton.setBackground(Color.GRAY);
		mqttButton.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		mqttButton.setBounds(10, 132, 160, 52);
		add(mqttButton);
		
		amqpButton = new JButton("AMQP");
		amqpButton.setForeground(Color.WHITE);
		amqpButton.setBackground(Color.GRAY);
		amqpButton.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		amqpButton.setBounds(242, 132, 160, 52);
		add(amqpButton);
		
	}
	
	public void setController(ActionListener c) {
		mqttButton.addActionListener(c);
		mqttButton.setActionCommand(CONFIRMMQTT);
		
		amqpButton.addActionListener(c);
		amqpButton.setActionCommand(CONFIRMAMQP);
	}
	
}
