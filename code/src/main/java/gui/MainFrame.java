package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {
	
	private JPanel bg;
	private JPanel content;
	private JPanel menu;
	private JSeparator separator_1;
	private JPanel step1LabelPanel;
	private JPanel step2LabelPanel;
	private JPanel step3LabelPanel;
	private JPanel step4LabelPanel;
	private JPanel step5LabelPanel;
	private JPanel step6LabelPanel;
	private JPanel step7LabelPanel;
	
	private JLabel step1Label;
	private JLabel step2Label;
	private JLabel step3Label;
	private JLabel step4Label;
	private JLabel step5Label;
	private JLabel step6Label;
	private JLabel step7Label;
	private JLabel iconLabel;
	
	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 500);
		bg = new JPanel();
		bg.setBackground(Color.BLACK);
		bg.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(bg);
		bg.setLayout(null);
		
		content = new JPanel();
		content.setBackground(Color.BLACK);
		content.setBounds(0, 161, 412, 300);
		bg.add(content);
		
		menu = new JPanel();
		menu.setBounds(411, 0, 373, 461);
		menu.setBackground(Color.BLACK);
		bg.add(menu);
		menu.setLayout(null);
		
		JLabel title = new JLabel("Flextory");
		title.setHorizontalAlignment(SwingConstants.CENTER);
		title.setFont(new Font("Segoe UI", Font.PLAIN, 24));
		title.setForeground(Color.WHITE);
		title.setBounds(122, 70, 128, 32);
		menu.add(title);
		
		JSeparator separator = new JSeparator();
		separator.setBackground(Color.WHITE);
		separator.setForeground(Color.WHITE);
		separator.setBounds(84, 53, -46, 39);
		menu.add(separator);
		
		separator_1 = new JSeparator();
		separator_1.setForeground(Color.WHITE);
		separator_1.setBounds(85, 113, 196, 8);
		menu.add(separator_1);
		
		step1LabelPanel = new JPanel();
		step1LabelPanel.setBackground(Color.LIGHT_GRAY);
		step1LabelPanel.setBounds(0, 196, 373, 39);
		menu.add(step1LabelPanel);
		step1LabelPanel.setLayout(null);
		
		step1Label = new JLabel("Step 1: Type consumer name");
		step1Label.setFont(new Font("Segoe UI", Font.BOLD, 14));
		step1Label.setForeground(Color.WHITE);
		step1Label.setBounds(60, 0, 313, 39);
		step1LabelPanel.add(step1Label);
		
		step2LabelPanel = new JPanel();
		step2LabelPanel.setBackground(Color.DARK_GRAY);
		step2LabelPanel.setBounds(0, 232, 373, 39);
		menu.add(step2LabelPanel);
		step2LabelPanel.setLayout(null);
		
		step2Label = new JLabel("Step 2: Enter JSON schema");
		step2Label.setFont(new Font("Segoe UI", Font.BOLD, 14));
		step2Label.setForeground(Color.WHITE);
		step2Label.setBounds(60, 0, 313, 39);
		step2LabelPanel.add(step2Label);
		
		step3LabelPanel = new JPanel();
		step3LabelPanel.setBackground(Color.DARK_GRAY);
		step3LabelPanel.setBounds(0, 271, 373, 39);
		menu.add(step3LabelPanel);
		step3LabelPanel.setLayout(null);
		
		step3Label = new JLabel("Step 3: Save algorithm template");
		step3Label.setFont(new Font("Segoe UI", Font.BOLD, 14));
		step3Label.setForeground(Color.WHITE);
		step3Label.setBounds(60, 0, 313, 39);
		step3LabelPanel.add(step3Label);
		
		step4LabelPanel = new JPanel();
		step4LabelPanel.setBackground(Color.DARK_GRAY);
		step4LabelPanel.setBounds(0, 308, 373, 39);
		menu.add(step4LabelPanel);
		step4LabelPanel.setLayout(null);
		
		step4Label = new JLabel("Step 4: Enter completed algorithm class");
		step4Label.setFont(new Font("Segoe UI", Font.BOLD, 14));
		step4Label.setForeground(Color.WHITE);
		step4Label.setBounds(60, 0, 313, 39);
		step4LabelPanel.add(step4Label);
		
		step5LabelPanel = new JPanel();
		step5LabelPanel.setBackground(Color.DARK_GRAY);
		step5LabelPanel.setBounds(0, 346, 373, 39);
		menu.add(step5LabelPanel);
		step5LabelPanel.setLayout(null);
		
		step5Label = new JLabel("Step 5: Choose external JARs");
		step5Label.setForeground(Color.WHITE);
		step5Label.setFont(new Font("Segoe UI", Font.BOLD, 14));
		step5Label.setBounds(60, 0, 313, 39);
		step5LabelPanel.add(step5Label);
		
		step6LabelPanel = new JPanel();
		step6LabelPanel.setBackground(Color.DARK_GRAY);
		step6LabelPanel.setBounds(0, 383, 373, 39);
		menu.add(step6LabelPanel);
		step6LabelPanel.setLayout(null);
		
		step6Label = new JLabel("Step 6: Select communication protocol");
		step6Label.setForeground(Color.WHITE);
		step6Label.setFont(new Font("Segoe UI", Font.BOLD, 14));
		step6Label.setBounds(60, 0, 313, 39);
		step6LabelPanel.add(step6Label);
		
		step7LabelPanel = new JPanel();
		step7LabelPanel.setBackground(Color.DARK_GRAY);
		step7LabelPanel.setBounds(0, 422, 373, 39);
		menu.add(step7LabelPanel);
		step7LabelPanel.setLayout(null);
		
		step7Label = new JLabel("Step 7: Save consumer");
		step7Label.setForeground(Color.WHITE);
		step7Label.setFont(new Font("Segoe UI", Font.BOLD, 13));
		step7Label.setBounds(60, 0, 313, 39);
		step7LabelPanel.add(step7Label);
		
		iconLabel = new JLabel("");
		iconLabel.setIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage(this.getClass().getClassLoader().getResource("resources/icon.png")))); //"resources/icon.png" It is the route of the icon in the JAR file
		
		iconLabel.setForeground(Color.WHITE);
		iconLabel.setBounds(0, 0, 209, 162);
		bg.add(iconLabel);
		
		Image icon = Toolkit.getDefaultToolkit().getImage(this.getClass().getClassLoader().getResource("resources/icon.png")); //"resources/icon.png" It is the route of the icon in the JAR file
		this.setIconImage(icon);    
	}
	
	public void changeStep(int stepNumber) {
        switch (stepNumber) {
            case 2:  
            	step1LabelPanel.setBackground(Color.DARK_GRAY);
            	step2LabelPanel.setBackground(Color.LIGHT_GRAY);
                break;
            case 3:  
            	step2LabelPanel.setBackground(Color.DARK_GRAY);
            	step3LabelPanel.setBackground(Color.LIGHT_GRAY);
                break;
            case 4:  
            	step3LabelPanel.setBackground(Color.DARK_GRAY);
            	step4LabelPanel.setBackground(Color.LIGHT_GRAY);
                break;
            case 5: 
            	step4LabelPanel.setBackground(Color.DARK_GRAY);
            	step5LabelPanel.setBackground(Color.LIGHT_GRAY);
                break;
            case 6: 
            	step5LabelPanel.setBackground(Color.DARK_GRAY);
            	step6LabelPanel.setBackground(Color.LIGHT_GRAY);
                break;
            case 7:  
            	step6LabelPanel.setBackground(Color.DARK_GRAY);
            	step7LabelPanel.setBackground(Color.LIGHT_GRAY);
                break;
        }
	}
	
	public void changePanel(JPanel newcontent) {
		newcontent.setPreferredSize(new Dimension(412, 350));
		content.removeAll();
		content.add(newcontent);
		content.revalidate();
		content.repaint();	
	}
}
