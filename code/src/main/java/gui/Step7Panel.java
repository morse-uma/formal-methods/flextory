package gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class Step7Panel extends JPanel {
	private JLabel consumerPathLabel;
	private JLabel emptyPathErrorLabel;
	
	private JButton showConsumerPathButton;
	private JButton selectConsumerPathButton;
	private JButton confirmConsumerPathButton;
	
	private JSeparator separator;
	
	private String consumerPath;
	
	public final String SELECTCONSUMERPATH = "selectConsumerPath";
	public final String CONFIRMCONSUMERPATH = "confirmConsumerPath";

	public Step7Panel() {
		setBackground(Color.BLACK);
		setForeground(Color.WHITE);
		setLayout(null);
		
		consumerPathLabel = new JLabel("Select a path to store the consumer");
		consumerPathLabel.setForeground(Color.WHITE);
		consumerPathLabel.setHorizontalAlignment(SwingConstants.CENTER);
		consumerPathLabel.setFont(new Font("Segoe UI", Font.BOLD, 14));
		consumerPathLabel.setBounds(10, 11, 392, 52);
		add(consumerPathLabel);
		
		showConsumerPathButton = new JButton("Path");
		showConsumerPathButton.setBackground(Color.BLACK);
		showConsumerPathButton.setForeground(Color.WHITE);
		showConsumerPathButton.setFont(new Font("Segoe UI Light", Font.PLAIN, 14));
		showConsumerPathButton.setEnabled(false);
		showConsumerPathButton.setBounds(10, 74, 392, 23);
		add(showConsumerPathButton);
		
		selectConsumerPathButton = new JButton("Open chooser");
		selectConsumerPathButton.setForeground(Color.WHITE);
		selectConsumerPathButton.setBackground(Color.GRAY);
		selectConsumerPathButton.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		selectConsumerPathButton.setBounds(10, 126, 127, 23);
		add(selectConsumerPathButton);
		
		separator = new JSeparator();
		separator.setBackground(Color.DARK_GRAY);
		separator.setForeground(Color.DARK_GRAY);
		separator.setBounds(10, 175, 392, 2);
		add(separator);
		
		confirmConsumerPathButton = new JButton("Confirm");
		confirmConsumerPathButton.setForeground(Color.WHITE);
		confirmConsumerPathButton.setBackground(Color.GRAY);
		confirmConsumerPathButton.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		confirmConsumerPathButton.setBounds(136, 266, 127, 23);
		add(confirmConsumerPathButton);
		
		emptyPathErrorLabel = new JLabel("Use the chooser to select where to save the consumer");
		emptyPathErrorLabel.setForeground(Color.RED);
		emptyPathErrorLabel.setHorizontalAlignment(SwingConstants.CENTER);
		emptyPathErrorLabel.setFont(new Font("Segoe UI Black", Font.BOLD, 11));
		emptyPathErrorLabel.setBackground(Color.RED);
		emptyPathErrorLabel.setBounds(10, 213, 392, 23);
		emptyPathErrorLabel.setVisible(false);
		add(emptyPathErrorLabel);
		setVisible(true);
	}
	
	public void setController(ActionListener c) {
		selectConsumerPathButton.addActionListener(c);
		selectConsumerPathButton.setActionCommand(SELECTCONSUMERPATH);
		
		confirmConsumerPathButton.addActionListener(c);
		confirmConsumerPathButton.setActionCommand(CONFIRMCONSUMERPATH);
	}
	
	public String searchConsumerPath() {
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setDialogTitle("Select where to store the consumer");
		int option = chooser.showSaveDialog(this);
		if (option == JFileChooser.APPROVE_OPTION) { 
			consumerPath = chooser.getSelectedFile().getAbsolutePath(); 
			showConsumerPathButton.setText(consumerPath);
			emptyPathErrorLabel.setVisible(false);
		}
		return consumerPath;
	}

	public void setErrorPath() {
		emptyPathErrorLabel.setVisible(true);	
	}
	
}
