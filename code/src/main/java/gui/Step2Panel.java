package gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class Step2Panel extends JPanel {
	private JLabel schemaPathLabel;
	private JLabel emptyPathErrorLabel;
	
	private JButton showSchemaPathButton;
	private JButton selectSchemaPathButton;
	private JButton confirmSchemaPathButton;
	
	private JSeparator separator;

	private String schemaPath;
	
	public final String SELECTSCHEMA = "selectSchemaPath";
	public final String CONFIRMSCHEMAPATH = "confirmSchemaPath";
	
	public Step2Panel() {
		setBackground(Color.BLACK);
		setForeground(Color.WHITE);
		setLayout(null);
		
		schemaPathLabel = new JLabel("Select the schema path");
		schemaPathLabel.setForeground(Color.WHITE);
		schemaPathLabel.setHorizontalAlignment(SwingConstants.CENTER);
		schemaPathLabel.setFont(new Font("Segoe UI", Font.BOLD, 18));
		schemaPathLabel.setBounds(10, 11, 392, 52);
		add(schemaPathLabel);
		
		showSchemaPathButton = new JButton("Path");
		showSchemaPathButton.setBackground(Color.BLACK);
		showSchemaPathButton.setForeground(Color.WHITE);
		showSchemaPathButton.setFont(new Font("Segoe UI Light", Font.PLAIN, 14));
		showSchemaPathButton.setEnabled(false);
		showSchemaPathButton.setBounds(10, 74, 392, 23);
		add(showSchemaPathButton);
		
		selectSchemaPathButton = new JButton("Open chooser");
		selectSchemaPathButton.setForeground(Color.WHITE);
		selectSchemaPathButton.setBackground(Color.GRAY);
		selectSchemaPathButton.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		selectSchemaPathButton.setBounds(10, 126, 127, 23);
		add(selectSchemaPathButton);
		
		separator = new JSeparator();
		separator.setBackground(Color.DARK_GRAY);
		separator.setForeground(Color.DARK_GRAY);
		separator.setBounds(10, 175, 392, 2);
		add(separator);
		
		confirmSchemaPathButton = new JButton("Confirm");
		confirmSchemaPathButton.setForeground(Color.WHITE);
		confirmSchemaPathButton.setBackground(Color.GRAY);
		confirmSchemaPathButton.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		confirmSchemaPathButton.setBounds(136, 266, 127, 23);
		add(confirmSchemaPathButton);
		
		emptyPathErrorLabel = new JLabel("Use the chooser to select the schema path");
		emptyPathErrorLabel.setForeground(Color.RED);
		emptyPathErrorLabel.setHorizontalAlignment(SwingConstants.CENTER);
		emptyPathErrorLabel.setFont(new Font("Segoe UI Black", Font.BOLD, 14));
		emptyPathErrorLabel.setBackground(Color.RED);
		emptyPathErrorLabel.setBounds(10, 213, 392, 23);
		emptyPathErrorLabel.setVisible(false);
		add(emptyPathErrorLabel);
		setVisible(true);
	}
	
	public void setController(ActionListener c) {
		selectSchemaPathButton.addActionListener(c);
		selectSchemaPathButton.setActionCommand(SELECTSCHEMA);
		
		confirmSchemaPathButton.addActionListener(c);
		confirmSchemaPathButton.setActionCommand(CONFIRMSCHEMAPATH);
	}
	
	public String searchSchema() {
		JFileChooser chooser = new JFileChooser();
		int option = chooser.showOpenDialog(this);
		if (option == JFileChooser.APPROVE_OPTION) { 
			schemaPath = chooser.getSelectedFile().getAbsolutePath(); 
			showSchemaPathButton.setText(schemaPath);
			emptyPathErrorLabel.setVisible(false);
		}
		return schemaPath;
	}

	public void setErrorPath() {
		emptyPathErrorLabel.setVisible(true);	
	}
	
}
