package gui;

import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class ResultPanel extends JPanel {
	JTextArea result;
	JScrollPane scrollRes;

	public ResultPanel() {
		setBackground(Color.BLACK);
		setForeground(new java.awt.Color(248, 249, 251));
		setLayout(null);
		
		result = new JTextArea();
		result.setLineWrap(true);
		result.setEditable(false); 
		result.setBackground(Color.WHITE);
		scrollRes = new JScrollPane(result, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollRes.setBounds(10, 11, 392, 278);
		add(scrollRes);
	}	
	
	public void showResult(String res) {
		result.setText(res);
	}
}
