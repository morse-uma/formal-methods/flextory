package gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class Step4Panel extends JPanel {
	private JLabel uploadAlgorithmLabel;
	private JLabel emptyPathErrorLabel;
	
	private JButton showAlgorithmPath;
	private JButton uploadAlgorithmButton;
	private JButton confirmAlgorithmButton;
	
	private JSeparator separator;
	
	private String algorithmPath;
	public final String SELECTALGORITHM = "selectAlgorithm";
	public final String CONFIRMALGOTIRHM = "confirmAlgorithm";

	public Step4Panel() {
		setBackground(Color.BLACK);
		setForeground(new java.awt.Color(248, 249, 251));
		setLayout(null);
		
		uploadAlgorithmLabel = new JLabel("Select the algorithm path");
		uploadAlgorithmLabel.setForeground(Color.WHITE);
		uploadAlgorithmLabel.setHorizontalAlignment(SwingConstants.CENTER);
		uploadAlgorithmLabel.setFont(new Font("Segoe UI", Font.BOLD, 18));
		uploadAlgorithmLabel.setBounds(10, 11, 392, 52);
		add(uploadAlgorithmLabel);
		
		showAlgorithmPath = new JButton("Path");
		showAlgorithmPath.setBackground(Color.BLACK);
		showAlgorithmPath.setForeground(Color.WHITE);
		showAlgorithmPath.setFont(new Font("Segoe UI Light", Font.PLAIN, 14));
		showAlgorithmPath.setEnabled(false);
		showAlgorithmPath.setBounds(10, 74, 392, 23);
		add(showAlgorithmPath);
		
		uploadAlgorithmButton = new JButton("Open chooser");
		uploadAlgorithmButton.setForeground(Color.WHITE);
		uploadAlgorithmButton.setBackground(Color.GRAY);
		uploadAlgorithmButton.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		uploadAlgorithmButton.setBounds(10, 126, 127, 23);
		add(uploadAlgorithmButton);
		
		separator = new JSeparator();
		separator.setBackground(Color.DARK_GRAY);
		separator.setForeground(Color.DARK_GRAY);
		separator.setBounds(10, 175, 392, 2);
		add(separator);
		
		confirmAlgorithmButton = new JButton("Confirm");
		confirmAlgorithmButton.setForeground(Color.WHITE);
		confirmAlgorithmButton.setBackground(Color.GRAY);
		confirmAlgorithmButton.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		confirmAlgorithmButton.setBounds(136, 266, 127, 23);
		add(confirmAlgorithmButton);
		
		emptyPathErrorLabel = new JLabel("Use the chooser the select the algorithm");
		emptyPathErrorLabel.setForeground(Color.RED);
		emptyPathErrorLabel.setHorizontalAlignment(SwingConstants.CENTER);
		emptyPathErrorLabel.setFont(new Font("Segoe UI Black", Font.BOLD, 14));
		emptyPathErrorLabel.setBackground(Color.RED);
		emptyPathErrorLabel.setBounds(10, 213, 392, 23);
		emptyPathErrorLabel.setVisible(false);
		add(emptyPathErrorLabel);
		setVisible(true);
	}
	
	public void setController(ActionListener c) {
		uploadAlgorithmButton.addActionListener(c);
		uploadAlgorithmButton.setActionCommand(SELECTALGORITHM);
		
		confirmAlgorithmButton.addActionListener(c);
		confirmAlgorithmButton.setActionCommand(CONFIRMALGOTIRHM);
	}
	
	public String searchAlgorithm() {
		JFileChooser chooser = new JFileChooser();
		int option = chooser.showOpenDialog(this);
		if (option == JFileChooser.APPROVE_OPTION) { 
			algorithmPath = chooser.getSelectedFile().getAbsolutePath(); 
			showAlgorithmPath.setText(algorithmPath);
			emptyPathErrorLabel.setVisible(false);
		}
		return algorithmPath;
	}

	public void setErrorPath() {
		emptyPathErrorLabel.setVisible(true);	
	}
	
}
