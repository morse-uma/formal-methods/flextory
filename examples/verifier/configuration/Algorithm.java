import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.mindrot.jbcrypt.BCrypt;


import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Algorithm implements Runnable {
	LinkedBlockingQueue<VerifierSchema> data;
	
	public Algorithm(LinkedBlockingQueue<VerifierSchema> data){
		this.data = data;
	}

	@Override
	public void run() {
		try {
			PrintWriter outputFile = new PrintWriter(new FileWriter("verifier.txt", true));
			for (VerifierSchema vs : data) {
				 String algorithm = vs.getAlgorithm();
				 String password = vs.getPassword();
				 String hash = vs.getHash();
				 
				 
				try {
					outputFile.println("Algorithm "+algorithm+":");
					outputFile.println("Password: "+ password);
					outputFile.println("Hash: "+ hash);
					if (algorithm.equals("MD5")) {
						String md5PasswordHex = DigestUtils.md5Hex(password);
						byte[] encoded = Base64.encodeBase64(md5PasswordHex.getBytes());
						String res = new String(encoded);
						if(hash.equals(res)) {
							outputFile.println("OK");
						}else {
							outputFile.println("Hash and password don´t match");
						}
					} else if (algorithm.equals("SHA-1")) {
						String SHA1PasswordHex = DigestUtils.sha1Hex(password);
						byte[] encoded = Base64.encodeBase64(SHA1PasswordHex.getBytes());
						String res = new String(encoded);
						if(hash.equals(res)) {
							outputFile.println("OK");
						}else {
							outputFile.println("Hash and password don´t match");
						}
					} else if (algorithm.equals("SHA-256")) {
						String SHA256PasswordHex = DigestUtils.sha256Hex(password);
						byte[] encoded = Base64.encodeBase64(SHA256PasswordHex.getBytes());
						String res = new String(encoded);
						if(hash.equals(res)) {
							outputFile.println("OK");
						}else {
							outputFile.println("Hash and password don´t match");
						}
					} else if (algorithm.equals("SHA-512")) {
						String SHA512PasswordHex = DigestUtils.sha512Hex(password);
						byte[] encoded = Base64.encodeBase64(SHA512PasswordHex.getBytes());
						String res = new String(encoded);
						if(hash.equals(res)) {
							outputFile.println("OK");
						}else {
							outputFile.println("Hash and password don´t match");
						}
					} else if (algorithm.equals("Bcrypt")) {
						if (BCrypt.checkpw(password, hash)) { 
							outputFile.println("OK");
						}else {
							outputFile.println("Hash and password don´t match");
						}
					}
					outputFile.println("------------------------------------------------");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			outputFile.close();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	} 
}