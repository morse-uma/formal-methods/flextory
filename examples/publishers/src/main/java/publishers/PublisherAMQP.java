package publishers;

import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;


import org.apache.commons.io.IOUtils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class PublisherAMQP {

	  private static final String TASK_QUEUE_NAME = "iris";

	  public static void main(String[] argv) throws Exception {
	   
		ConnectionFactory factory = new ConnectionFactory();
	    
		factory.setHost("localhost");
		//factory.setPort(667);
		//factory.setUsername("");
		//factory.setPassword("");
		
	    
		InputStream jsonData;

		try {
			
			Connection connection = factory.newConnection();
			
			System.out.println("Connected");

			Channel channel = connection.createChannel();
			
			boolean durable = false;

			channel.queueDeclare(TASK_QUEUE_NAME, durable, false, false, null);

			jsonData = new FileInputStream("iris.json");
			String json = IOUtils.toString(jsonData, StandardCharsets.UTF_8);
			
			channel.basicPublish("",TASK_QUEUE_NAME, null, json.getBytes());
			System.out.println("Message published");
			
			connection.close();
			System.out.println("Disconnected");
			
			System.exit(0);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}

