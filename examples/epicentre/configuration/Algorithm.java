import java.util.concurrent.LinkedBlockingQueue;
import java.util.ArrayList;
import java.util.Arrays;
import java.io.FileWriter;
import java.io.PrintWriter;

public class Algorithm implements Runnable {
	LinkedBlockingQueue<EpiSchema> data;
	private final static String log_filename = "output.txt";
	
	public Algorithm(LinkedBlockingQueue<EpiSchema> data){
		this.data = data;
	}

	@Override
	public void run() {
		ArrayList<String> category = new  ArrayList<String>(Arrays.asList("5g_network", "nfv_mano", "vnf_chain", "experiment"));
		ArrayList<String> origin =  new  ArrayList<String>(Arrays.asList("UE", "RAN", "5GC", "EPC", "main data server", "edge"));
		try {
			PrintWriter outputFile = new PrintWriter(new FileWriter(log_filename, true));
			for(EpiSchema message : data) {
				if (!category.contains(message.getCategory())) {
					outputFile.println("***Invalid or missing category***");
					outputFile.println(message.toString());
				}  
				if (message.getTestbedId() == null) {
					outputFile.println("***Missing testbed ID***");
				}  
				
				for (Data d : message.getData()) {
					if (d.getType() == null || d.getTimestamp() == null || !origin.contains(d.getOrigin())) {
						outputFile.println("***Missing one or more required data arguments**");
						break;
					}
				}
				
				outputFile.println("category: " + message.getCategory());
				outputFile.println("testbed_id: " + message.getTestbedId());
				outputFile.println("scenario_id: " + message.getScenarioId());
				outputFile.println("use_case_id: " + message.getUseCaseId());
				outputFile.println("experiment_id: " + message.getExperimentId());
				outputFile.println("netapp_id: " + message.getNetappId());
				outputFile.println("data: [");
				for (Data d : message.getData()) {
					outputFile.println("{");
					outputFile.println("	type: " + d.getType());
					outputFile.println("	timestamp: " + d.getTimestamp());
					outputFile.println("	origin: " + d.getOrigin());
					outputFile.println("	unit: " + d.getUnit());
					for(var entry : d.getAdditionalProperties().entrySet()) {
						outputFile.println("	" + entry.getKey() + " : " + entry.getValue());
					}
					outputFile.println("}");
				}
				outputFile.println("]");
				outputFile.println("--------------------------------------------------");
			}
			outputFile.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	} 
}
