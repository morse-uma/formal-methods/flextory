import java.sql.Timestamp;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import learning.LearningAlgorithm;
import trace.Observation;
import trace.Trace;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.concurrent.ThreadPoolExecutor;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class Algorithm implements Runnable {
	LinkedBlockingQueue<DashSchema> data;
	LearningAlgorithm la = new LearningAlgorithm(2);

	public Algorithm(LinkedBlockingQueue<DashSchema> data) {
		this.data = data;
	}

	private class MyHttpHandler implements HttpHandler {
		@Override
		public void handle(HttpExchange httpExchange) throws IOException {
			String requestParamValue = null;

			OutputStream outputStream = httpExchange.getResponseBody();
			String htmlResponse = la.show2();
			//System.out.println(htmlResponse);
			// encode HTML content

			// this line is a must
			httpExchange.sendResponseHeaders(200, htmlResponse.length());
			outputStream.write(htmlResponse.getBytes());
			outputStream.flush();
			outputStream.close();
		}

	}

	@Override
	public void run() {

		try {
			HttpServer server = HttpServer.create(new InetSocketAddress(8001), 0);
			ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
			server.createContext("/", new MyHttpHandler());
			server.setExecutor(threadPoolExecutor);
			server.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ArrayList<Trace> traces = new ArrayList<>();
		int time = 0;
		long lastTime = 0;
		Timestamp lastTimestamp = null;
		Trace t;
		int lastSeqN = -1;
		ArrayList<Observation> observations = new ArrayList<>();
		while (true) {

			///////////////////////////

			if (data.size() > 0) {
				DashSchema record = data.remove();
				// System.out.println(record);

				for (var entry : record.getAdditionalProperties().entrySet()) {
					if (entry.getKey().equals("end")) {
						if (!observations.isEmpty()) {
							t = new Trace(observations);
							traces.add(t);
							la.learnNewTraces(traces);
							la.stopLearning();
							la.show();
						}
						return;
					}
					;
				}

				var currentTimestamp = Timestamp.valueOf(record.getTimestamp());
				var flags = record.getFlags();
				var sourcePort = record.getSource();
				var targetPort = record.getTarget();
				var seqN = record.getSeqN();

				if (lastSeqN > 0 && seqN == 0) {
					System.out.println("aqui");
					t = new Trace(observations);
					traces.add(t);
					la.learnNewTraces(traces);
					observations = new ArrayList<>();
					time = 0;
					lastTime = 0;
					lastTimestamp = null;
				}

				if (lastTimestamp != null) {
					lastTime = Duration.between(lastTimestamp.toInstant(), currentTimestamp.toInstant()).toNanos();
				}
				lastTimestamp = currentTimestamp;
				time += lastTime;
				lastSeqN = seqN;

				String eventString = "";

				switch (flags) {
				case 2:
					eventString = "syn";
					break;
				case 4:
					eventString = "rst";
					break;
				case 10:
					eventString = "ack";
					break;
				case 11:
					eventString = "fin_ack";
					break;
				case 12:
					eventString = "syn_ack";
					break;
				case 14:
					eventString = "rst_ack";
					break;
				case 18:
					eventString = "psh_ack";
					break;
				case 19:
					eventString = "fin_psh_ack";
					break;
				}

				ArrayList<String> variables = new ArrayList();

				if (seqN == 0) {
					variables.add("ConnInit");
				}

				Observation observation = new Observation(time, eventString, variables);
				observations.add(observation);

			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//////////////////////////
		}
	}
}