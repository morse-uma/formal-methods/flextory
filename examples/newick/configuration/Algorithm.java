import java.util.concurrent.LinkedBlockingQueue;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

import algorithms.sankoff.Sankoff;
import tree.PhylogeneticTree;

public class Algorithm implements Runnable {
	LinkedBlockingQueue<NewickSchema> data;
    private final static String log_filename = "sankoff_log.txt";
	public Algorithm(LinkedBlockingQueue<NewickSchema> data){
		this.data = data;
	}

	@Override
	public void run() {
		try {
				PrintWriter outputFile = new PrintWriter(new FileWriter(log_filename, true));
				for(NewickSchema n : data) {
					String label = n.getNewick();
					int id = n.getTreeId();
					Sankoff.labelLength = n.getLabelLength();
					PhylogeneticTree t = PhylogeneticTree.readFromString(label);
					//Copy in the file the obtained tree
					outputFile.println("Id: "+ id+" original tree: "+t.toString());
						
					int parsimonyScore = Sankoff.computeParsimonyScore(t);
					//Copy in the file the parsimony score
					outputFile.println("Id: "+ id+" Parsimony Score: "+parsimonyScore);
					outputFile.println("Id: "+ id+" full-labeled Tree: "+t);
				}
				outputFile.close();

				} catch (Exception e) {
					e.printStackTrace();
				}
	} 
}