import java.util.concurrent.LinkedBlockingQueue;

public class Algorithm implements Runnable {
	LinkedBlockingQueue<IrisSchema> data;
	
	public Algorithm(LinkedBlockingQueue<IrisSchema> data){
		this.data = data;
	}

	@Override
	public void run() {
		float virginica = 0;
		float versicolor = 0;
		float setosa = 0;
		float other = 0;
		for (IrisSchema flower : data) {
			switch (flower.getSpecies()) {
			case "setosa":
				setosa++;
				break;
			case "versicolor":
				versicolor++;
				break;
			case "virginica":
				virginica++;
				break;
			default:
				other++;
			}
		}
		System.out.println("Virginica: " + virginica / data.size() * 100 + "%");
		System.out.println("Versicolor: " + versicolor / data.size() * 100 + "%");
		System.out.println("Setosa: " + setosa / data.size() * 100 + "%");
		System.out.println("Null: " + other / data.size() * 100 + "%");	
	} 
}
